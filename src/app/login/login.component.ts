import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  closeResult: string;
  email = '';
  password = '';

  constructor(private modalService: NgbModal, private router: Router) {}

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    this.exitLogin();
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  private exitLogin() {
    if ((this.email === 'semear.devtest@gmail.com') && (this.password === 'strong@pass#42')) { 
      environment.isLogged = true;
      this.router.navigate(['home']);
    }
  }

  enter(){
    this.modalService.dismissAll();
  }

  adm(){
    environment.isLogged = true;
    this.router.navigate(['home-adm']);
  }

  ngOnInit() {
  }

}
