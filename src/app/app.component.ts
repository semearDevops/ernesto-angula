import { Component, AfterViewInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
styleUrls: ['./app.component.css']
})
export class AppComponent {

  closeResult: string;

  constructor(private modalService: NgbModal){}

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openHelp(content2) {
    this.modalService.open(content2, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openConfig(content3) {
    this.modalService.open(content3, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
 
  }

  enter(){
    this.modalService.dismissAll();
  }

  title = 'labs-contaDigital-angular';
  urlInicio = 'assets/imgs/icons/ic_inicio.svg';
  urlConfig = 'assets/imgs/icons/ic_config.svg';
  urlAjuda = 'assets/imgs/icons/ic_ajuda.svg';
  urlDados ='assets/imgs/icons/ic_dados_pessoais.svg';
  urlEndereco = 'assets/imgs/icons/ic_endereco.svg';
  urlFinanceiras = 'assets/imgs/icons/ic_financeiras.svg';
  urlId = 'assets/imgs/icons/ic_docs_identificacao.svg';
  urlTarifas = 'assets/imgs/icons/ic_pct_tarifas.svg';

  personalData = true;
  financialInformation = false;
  identificationDocuments = false;
  ratePackage = false;
  address = false;
  home = false;
  config = false;
  help = false;

  toggleSidebar(){
    document.getElementById('sidebar').classList.toggle('active');
  }

  get sideActive(){
    return document.getElementById('sidebar').classList.contains('active');
  }

  toggleSidebarOff(){
    document.getElementById('sidebar').classList.toggle('active');
  }

  dataActive(){
    this.personalData = true;
    this.financialInformation = false;
    this.identificationDocuments = false;
    this.ratePackage = false;
    this.address = false;
    this.home = false;
    this.config = false;
    this.help = false;
  }

  addressActive(){
    this.address = true;
    this.personalData = false;
    this.financialInformation = false;
    this.identificationDocuments = false;
    this.ratePackage = false;
    this.home = false;
    this.config = false;
    this.help = false;
  }

  financialActive(){
    this.personalData = false;
    this.financialInformation = true;
    this.identificationDocuments = false;
    this.ratePackage = false;
    this.address = false;
    this.home = false;
    this.config = false;
    this.help = false;
  }

  identificationActive(){
    this.personalData = false;
    this.financialInformation = false;
    this.identificationDocuments = true;
    this.ratePackage = false;
    this.address = false;
    this.home = false;
    this.config = false;
    this.help = false;
  }

  rateActive(){
    this.personalData = false;
    this.financialInformation = false;
    this.identificationDocuments = false;
    this.ratePackage = true;
    this.address = false;
    this.home = false;
    this.config = false;
    this.help = false;
  }

  homeActive(){
    this.personalData = false;
    this.financialInformation = false;
    this.identificationDocuments = false;
    this.ratePackage = false;
    this.address = false;
    this.home = true;
    this.config = false;
    this.help = false;
  }

  configActive(){
    this.personalData = false;
    this.financialInformation = false;
    this.identificationDocuments = false;
    this.ratePackage = false;
    this.address = false;
    this.home = false;
    this.config = true;
    this.help = false;
  }

  helpActive(){
    this.personalData = false;
    this.financialInformation = false;
    this.identificationDocuments = false;
    this.ratePackage = false;
    this.address = false;
    this.home = false;
    this.config = false;
    this.help = true;
  }


  getLogged() {
    return environment.isLogged;
  }
}
