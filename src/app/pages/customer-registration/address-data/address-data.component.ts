import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'address-data',
  templateUrl: './address-data.component.html',
  styleUrls: ['./address-data.component.css']
})
export class AddressDataComponent implements OnInit, AfterViewInit  {
  address = false;

  constructor() { }

  ngOnInit() {  

  }

  ngAfterViewInit() {
  }

  enterAddress(){
    this.address = true;
  }

  cancelAddress(){
    this.address = false;
  }


}
