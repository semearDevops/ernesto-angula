import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
    document.getElementById('sidebar').classList.toggle('');
  }

  toggleSidebar(){
    document.getElementById('sidebar').classList.toggle('active');
    this.route.navigateByUrl('/register-personal-data');
  }

  toggleSidebarOff(){
    this.route.navigateByUrl('/performance-panel');
    document.getElementById('sidebar').classList.toggle('');
  }

}
