import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-finalized',
  templateUrl: './finalized.component.html',
  styleUrls: ['./finalized.component.css']
})
export class FinalizedComponent implements OnInit {

  // PieChart = [];

  constructor() { }

  ngOnInit() {
    // pie chart:
    let PieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        labels: ["Em andamento", "Finalizados"],
        datasets: [{
          label: '# of Votes',
          data: [15, 85],
          backgroundColor: [
            '#333f65',
            '#6eb8da'
          ],
          borderColor: [
           '#fff',
           'transparent'
          ],
          borderWidth: 3
        }]
      },
      options: {
        title: {
          text: "Bar Chart",
          display: false
        },
        legend: {
          display: false
      },
        scales: {
          yAxes: [{
            display : false,
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: false
            }
          }]
        }
      }
    });

  }  

}
