import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-performance-panel',
  templateUrl: './performance-panel.component.html',
  styleUrls: ['./performance-panel.component.css']
})
export class PerformancePanelComponent implements OnInit {

  // LineChart = [];
  // PieChart = [];


  constructor() { }

  ngOnInit() {

    // Line chart:
    let LineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: ["01/12","02/12","03/12","04/12","05/12","06/12","07/12","08/12","08/12","09/12","10/12","11/12","12/12","13/12","14/12","15/12","16/12","17/12","18/12","19/12","20/12","21/12","22/12","23/12","24/12","25/12",],
        datasets: [{
          label: 'Number of Items Sold in Months',
          data: [0, 5, 5, 11, 10, 1, 4, 1, 5, 5, 5, 5, 5, 5, 7, 8, 9, 10, 0, 5, 7, 10, 5, 5, 5, 5],
          fill: false,
          lineTension: 0.1,
          borderColor: "#333f65",
          borderWidth: 4,
          pointBackgroundColor: '#4478a7',
          pointBorderWidth: 2,
          pointRadius: 6,
        }]
      },
      options: {
        title: {
          text: "Line Chart",
          display: false
        },
        legend: {
          display: false
      },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              color: "#ccc"
            },
            scaleLabel: {
              display: true,
              labelString: "",
              fontColor: "",
            }
          }],
          yAxes: [{
            gridLines: {
              color: "#ccc",
              // borderDash: [2, 5],
            },
            scaleLabel: {
              display: true,
              labelString: "Quantidade de Cadastros",
              fontColor: "gray",
              padding: 10
            }
          }]
        }
      }
    });


    // pie chart:
    let PieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        labels: ["Em andamento", "Finalizados"],
        datasets: [{
          label: '# of Votes',
          data: [8, 47],
          backgroundColor: [
            '#e5cc38',
            '#6eb8da'
          ],
          borderColor: [
           
          ],
          borderWidth: 3
        }]
      },
      options: {
        title: {
          text: "Bar Chart",
          display: false
        },
        legend: {
          display: false
      },
        scales: {
          yAxes: [{
            display : false,
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: false
            }
          }]
        }
      }
    });

  }

}
