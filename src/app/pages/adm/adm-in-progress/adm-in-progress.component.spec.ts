import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmInProgressComponent } from './adm-in-progress.component';

describe('AdmInProgressComponent', () => {
  let component: AdmInProgressComponent;
  let fixture: ComponentFixture<AdmInProgressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmInProgressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmInProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
