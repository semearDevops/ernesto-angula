import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-adm-in-progress',
  templateUrl: './adm-in-progress.component.html',
  styleUrls: ['./adm-in-progress.component.css']
})
export class AdmInProgressComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    // pie chart:
    let PieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        labels: ["Em andamento", "Finalizados"],
        datasets: [{
          label: '# of Votes',
          data: [14, 86],
          backgroundColor: [
            '#e5cc38',
            '#333f65'
          ],
          borderColor: [
           '#e5cc38',
           '#fff'
          ],
          borderWidth: 3
        }]
      },
      options: {
        title: {
          text: "Bar Chart",
          display: false
        },
        legend: {
          display: false
      },
        scales: {
          yAxes: [{
            display : false,
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: false
            }
          }]
        }
      }
    });

  }  

}
