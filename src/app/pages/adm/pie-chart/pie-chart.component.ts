import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    // pie chart:
    let PieChart = new Chart('pieChart', {
      type: 'pie',
      data: {
        labels: ["Em andamento", "Finalizados"],
        datasets: [{
          label: '# of Votes',
          data: [8, 47],
          backgroundColor: [
            '#e5cc38',
            '#6eb8da'
          ],
          borderColor: [
           
          ],
          borderWidth: 3
        }]
      },
      options: {
        title: {
          text: "Bar Chart",
          display: false
        },
        legend: {
          display: false
      },
        scales: {
          yAxes: [{
            display : false,
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              display: false
            }
          }]
        }
      }
    });

  }

}

