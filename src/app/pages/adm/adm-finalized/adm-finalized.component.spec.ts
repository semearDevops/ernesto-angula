import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmFinalizedComponent } from './adm-finalized.component';

describe('AdmFinalizedComponent', () => {
  let component: AdmFinalizedComponent;
  let fixture: ComponentFixture<AdmFinalizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmFinalizedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmFinalizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
