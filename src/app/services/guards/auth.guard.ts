import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(): boolean {
    if (environment.isLogged) {
      return true
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}
