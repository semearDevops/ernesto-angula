import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { RegisterPersonalDataComponent } from './pages/customer-registration/register-personal-data/register-personal-data.component';
import { AddressDataComponent } from './pages/customer-registration/address-data/address-data.component';
import { FinancialInformationComponent } from './pages/customer-registration/financial-information/financial-information.component';
import { IdentificationDocumentsComponent } from './pages/customer-registration/identification-documents/identification-documents.component';
import { AuthGuard } from './services/guards/auth.guard';
import { RatePackageComponent } from './pages/customer-registration/rate-package/rate-package.component';
import { PerformancePanelComponent } from './pages/performance/performance-panel/performance-panel.component';
import { InProgressComponent } from './pages/performance/in-progress/in-progress.component';
import { FinalizedComponent } from './pages/performance/finalized/finalized.component';
import { HomeAdmComponent } from './pages/adm/home-adm/home-adm.component';
import { AdmFinalizedComponent } from './pages/adm/adm-finalized/adm-finalized.component';
import { AdmInProgressComponent } from './pages/adm/adm-in-progress/adm-in-progress.component';
import { SellerRegistrationComponent } from './pages/adm/seller-registration/seller-registration.component';

const routes: Routes = [

  {path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] }, 
  { path: 'nav-bar', component: NavBarComponent, canActivate: [AuthGuard] }, 
  { path: 'login', component: LoginComponent },
  { path: 'register-personal-data', component: RegisterPersonalDataComponent, canActivate: [AuthGuard] },
  { path: 'address-data', component: AddressDataComponent, canActivate: [AuthGuard] },
  { path: 'financial-information', component: FinancialInformationComponent, canActivate: [AuthGuard] },
  { path: 'identification-documents', component: IdentificationDocumentsComponent, canActivate: [AuthGuard] },
  { path: 'rate-package', component: RatePackageComponent, canActivate: [AuthGuard] },
  { path: 'performance-panel', component: PerformancePanelComponent, canActivate: [AuthGuard] },
  { path: 'in-progress', component: InProgressComponent, canActivate: [AuthGuard] },
  { path: 'finalized', component: FinalizedComponent, canActivate: [AuthGuard] },
  
  { path: 'home-adm', component: HomeAdmComponent, canActivate: [AuthGuard] },
  { path: 'adm-finalized', component: AdmFinalizedComponent, canActivate: [AuthGuard] },
  { path: 'adm-in-progress', component: AdmInProgressComponent, canActivate: [AuthGuard] },
  { path: 'seller-registration', component: SellerRegistrationComponent, canActivate: [AuthGuard] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
