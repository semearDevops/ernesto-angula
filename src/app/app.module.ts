import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UiSwitchModule } from 'ngx-toggle-switch';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { RegisterPersonalDataComponent } from './pages/customer-registration/register-personal-data/register-personal-data.component';
import { AddressDataComponent } from './pages/customer-registration/address-data/address-data.component';
import { FinancialInformationComponent } from './pages/customer-registration/financial-information/financial-information.component';
import { IdentificationDocumentsComponent } from './pages/customer-registration/identification-documents/identification-documents.component';
import { AuthGuard } from './services/guards/auth.guard';
import { RatePackageComponent } from './pages/customer-registration/rate-package/rate-package.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { PerformancePanelComponent } from './pages/performance/performance-panel/performance-panel.component';
import { InProgressComponent } from './pages/performance/in-progress/in-progress.component';
import { FinalizedComponent } from './pages/performance/finalized/finalized.component';
import { HomeAdmComponent } from './pages/adm/home-adm/home-adm.component';
import { PieChartComponent } from './pages/adm/pie-chart/pie-chart.component';
import { LineChartComponent } from './pages/adm/line-chart/line-chart.component';
import { AdmFinalizedComponent } from './pages/adm/adm-finalized/adm-finalized.component';
import { AdmInProgressComponent } from './pages/adm/adm-in-progress/adm-in-progress.component';
import { SellerRegistrationComponent } from './pages/adm/seller-registration/seller-registration.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LoginComponent,
    FooterComponent,
    HomeComponent,
    RegisterPersonalDataComponent,
    AddressDataComponent,
    FinancialInformationComponent,
    IdentificationDocumentsComponent,
    RatePackageComponent,
    SideMenuComponent,
    PerformancePanelComponent,
    InProgressComponent,
    FinalizedComponent,
    HomeAdmComponent,
    PieChartComponent,
    LineChartComponent,
    AdmFinalizedComponent,
    AdmInProgressComponent,
    SellerRegistrationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UiSwitchModule,
    NgbModule,
  ],
  providers: [
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
